# vkEngine

3D graphics engine in c++17 and Vulkan

For now, works only on Linux
Tested : Linux Mint 18

## compile 
You need :
* gcc 7.0 
* cmake 3.7
* Vulkan
* GLFW3.3
        
        
## Dependencies:
* glm :
    Mathematics Library design to be use with OpenGL
* utils :
    My personnal utils Library, it is a submodule for this project