//
// Created by stiven on 18-01-02.
//

#include <scene/rootNode.hpp>
namespace vkEngine {
	unsigned RootNode::nextId = 0;
	glm::mat4 RootNode::getView() { return camera->getView(); }

	glm::mat4 RootNode::getProj() { return camera->getProjection(); }

	void RootNode::changeCam(Camera const *newCam) {
		if (firstCam && camera) {
			delete camera;
			firstCam = false;
		}
		camera = newCam;
	}

	void RootNode::assignNumber(Node *node) {
		for(auto &child : *node){
			child->id = nextId++;
			assignNumber(child);
		}
	}

	void RootNode::assignNumber() {
		nextId = 0;
		assignNumber(this);
	}

	RootNode::~RootNode() {
		if (firstCam) {
			delete camera;

		}
	}

	void RootNode::update() {

	}
}