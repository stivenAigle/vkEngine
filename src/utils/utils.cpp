//
// Created by stiven on 18-01-01.
//
#include <utils/utils.h>
#include <vkEngine/scene/node.h>

namespace vkEngine {
	//TODO: think about an iterative method...maybe
	void forEachNode(std::function<void(Node *)> const &function, Node *node) {

		for (auto &child : *node) {
			forEachNode(function, child);
			function(child);
		}
	}


	void forEachNode(std::function<void(Node *)> const &function) {
		forEachNode(function, vkEngine::Node::getRootAsNode());
	}

}