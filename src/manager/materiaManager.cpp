//
// Created by stiven on 17-10-08.
//

#include <manager/materialManager.h>

namespace vkEngine{
     MaterialManager MaterialManager::materialManager;

    int MaterialManager::get(std::string &&pathMaterial) {
        return materialManager.materials.get(std::move(pathMaterial),[](std::string const & path, mapMaterials &add){
            parseMtl(path, add);
        });
    }


    mapMaterials MaterialManager::get(int i) {
        return materialManager.materials[i];
    }
}
