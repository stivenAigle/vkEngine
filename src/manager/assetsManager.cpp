//
// Created by stiven on 17-10-02.
//
#include <glish/Vao.hpp>
#include <utility>
#include <vkEngine/manager/textureManager.h>
#include <inc/vkEngine/manager/assetsManager.h>

namespace vkEngine {
    AssetsManager AssetsManager::assetsManager;


    void AssetsManager::changePath(std::string &&path) {
        pathModel = std::move(path);

    }

    int AssetsManager::getMaterials(std::string &&path) {
        return MaterialManager::get(std::move(path));
    }

    int AssetsManager::getModel(std::string &&a) {
        return MeshManager::get(std::move(a));
    }

    int AssetsManager::getTexture(std::string &&path) {
        return TextureManager::get(std::move(path));
    }
}