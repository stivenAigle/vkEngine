//
// Created by stiven on 17-10-11.
//

#include <vkEngine/manager/meshManager.h>
#include <vkEngine/parser/parser.h>
#include <memory>
#include <vkEngine/vulkan/initVulkan.hpp>

namespace vkEngine {
    MeshManager MeshManager::manager;
    int MeshManager::get(std::string &&path) {
        return manager.models.get(std::move(path), [](std::string const & pathToObj, mesh & currentMesh) {
            parsing_data data = vkEngine::parseObj(pathToObj);
            std::string tmp = pathToObj;
            currentMesh.verticesModel = vkEngine::InitVulkan::getInstance().createVertexBuffer(std::move(tmp));
            for(auto &[key, value] : data.materials){
                currentMesh.range.emplace_back(*value.first, *value.count);
            }
        });
        // OpenGL way
        /*return manager.models.get(std::move(path), [](std::string const & pathToObj){
            parsing_data data = vkEngine::parseObj(pathToObj);
            mesh currentMesh;
            glish::Vao<3> vao;
            vao.addVbo(0, data.vertices);
            vao.addVbo(1, data.uvs);
            vao.addVbo(2, data.normals);
            currentMesh.vao = vao;
            for(auto &[key, value] : data.materials){
                currentMesh.range.emplace_back(*value.first, *value.count);
            }
            return currentMesh;
        });*/
    }

    const mesh & MeshManager::get(int i) {
        return manager.models[i];
    }

    void MeshManager::use(int i) {
//        manager.models[i].vao.bind();
    }


}
