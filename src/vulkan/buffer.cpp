//
// Created by stiven on 17-12-30.
//

#include "vkEngine/vulkan/buffer.hpp"
namespace vkEngine{


	Buffer::Buffer(Buffer &&oldVertex) {

		emptyingBuffer(std::move(oldVertex));
	}

	Buffer &Buffer::operator=(Buffer &&oldVertex) {

		emptyingBuffer(std::move(oldVertex));
		return *this;
	}

	void Buffer::destroy() {
		if(buffer) {
			vkDestroyBuffer(device, buffer, nullptr);
			vkFreeMemory(device, memory, nullptr);
			buffer = 0;
			memory = 0;
		}
	}

	Buffer::~Buffer() {
		destroy();
	}

	void Buffer::emptyingBuffer(Buffer &&oldVertex) {
		if(buffer){
			destroy();
		}
		buffer = oldVertex.buffer;
		memory = oldVertex.memory;
		device = oldVertex.device;

		oldVertex.buffer = 0;
		oldVertex.memory = 0;
	}

	Buffer::Buffer(bufferData bufferInfo) : buffer(bufferInfo.buffer),
	                                         memory(bufferInfo.memory),
	                                         device(bufferInfo.device) {}
}