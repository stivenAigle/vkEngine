//
// Created by stiven on 17-09-25.
//

#include <vkEngine/baseClass/movableObject.h>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/orthonormalize.hpp>
#include <utils/glmAdd.h>

namespace vkEngine {
    MovableObject::MovableObject(glm::vec3 const &pos) : pos(pos), rotationCenter(pos) { }

    MovableObject::MovableObject(float x, float y, float z) : pos(x, y, z), rotationCenter(pos) {
    }

    void updateChangeAxis(glm::mat4 & mat, glm::vec3  const & axis, float line){
        mat[0][line] = axis.x;
        mat[1][line] = axis.y;
        mat[2][line] = axis.z;
    }


    void MovableObject::rotateX(float anglex, Axis axisCoordinate) {

        if(axisCoordinate != Axis::WORLD){
            rotate(rotateOwnAxis(anglex, worldAxis.xAxis));
        }else {
            rotate(glm::rotate(anglex, worldAxis.xAxis));

        }
    }

    void MovableObject::rotateY(float angley, Axis axisCoordinate) {
        if(axisCoordinate != Axis::WORLD){
            rotate(rotateOwnAxis(angley, worldAxis.yAxis));
        }else {
            rotate(glm::rotate(angley, worldAxis.yAxis));
        }

    }

    void MovableObject::rotateZ(float anglez, Axis axisCoordinate) {
        if(axisCoordinate != Axis::WORLD){
            rotate(rotateOwnAxis(anglez, worldAxis.zAxis));

        }else {
            rotate(glm::rotate(anglez, worldAxis.zAxis));
        }

    }

    MovableObject::~MovableObject() {

    }

    const glm::vec3 &MovableObject::getPos() const {
        return pos;
    }

    glm::mat4  MovableObject::getTransform() const{

	    return translation*rotation*glm::scale(scaling);


    }

    void MovableObject::rotate(glm::mat4 newRotate) {

        change(newRotate);
        glm::vec3 center = rotationCenter  - pos;
        if(!(glm::abs(center) <= glm::vec3{0.01f})) {
            newRotate = glm::translate(rotationCenter) * newRotate * glm::translate(-rotationCenter);
            pos = glm::vec3(newRotate*glm::vec4(pos, 1));
            oldRotationCenter = glm::vec3(newRotate*glm::vec4(oldRotationCenter, 1));
            if(moveWithBody){
                rotationCenter = glm::vec3(newRotate*glm::vec4(rotationCenter, 1));
            }
            rotation = newRotate * rotation;
        }else {
            rotation = rotation*newRotate;
        }

    }
/***
 *
 * @param delta : delta of movement
 * @param axis : axis of movement : must be normalize
 */
    void MovableObject::move(float delta, glm::vec3 const &axis) {
        move(delta*axis);
    }

    void MovableObject::change(glm::mat4 const &newRotation) {

            axe.xAxis = glm::normalize(glm::mat3(newRotation) * axe.xAxis);
            axe.yAxis = glm::normalize(glm::mat3(newRotation) * axe.yAxis);
            axe.zAxis = glm::normalize(glm::mat3(newRotation) * axe.zAxis);

            updateChangeAxis(changeAxis, axe.xAxis, 0);
            updateChangeAxis(changeAxis, axe.yAxis, 1);
            updateChangeAxis(changeAxis, axe.zAxis, 2);
            changeAxis = glm::mat4(glm::orthonormalize(glm::mat3(changeAxis)));
            transposeChangeAxis = glm::transpose(changeAxis);


    }

    glm::mat4 MovableObject::rotateOwnAxis(float value, glm::vec3 axis) {

        return transposeChangeAxis*glm::rotate(value, axis)*changeAxis;
    }

    void MovableObject::move(glm::vec3 translate) {
            translation[3] += glm::vec4(translate, 0);
            pos += translate;
            if(moveWithBody){
                rotationCenter += translate;
            }
    }

    void MovableObject::lockRotationCenter() {
        moveWithBody = false;
    }

    void MovableObject::unlockRotationCenter() {
        moveWithBody = true;
    }

    void MovableObject::changeRotationCenter(MovableObject const &object) {
        oldRotationCenter = rotationCenter;
        rotationCenter = object.pos;
    }

    void MovableObject::lastRotationCenter() {
        glm::vec3 temp = rotationCenter;
        rotationCenter = oldRotationCenter;
        oldRotationCenter = temp;
    }

    void MovableObject::scaleX(float scalex) {
        scaling.x *= scalex;
    }

    void MovableObject::scaleY(float scaley) {
        scaling.y *= scaley;

    }

    void MovableObject::scaleZ(float scalez) {
        scaling.z *= scalez;

    }

    void MovableObject::scale(float scaleAll) {
        scaleX(scaleAll);
        scaleY(scaleAll);
        scaleZ(scaleAll);
    }

    const axis &MovableObject::getAxis() const {
        return axe;
    }




}