//
// Created by stiven on 18-02-23.
//

#include <glm/gtc/matrix_transform.hpp>
#include "vkEngine/display/orthographicCamera.hpp"
namespace vkEngine {

	OrthographicCamera::OrthographicCamera(const glm::vec3 &pos,
	                                       const glm::vec3 &lookAt,
	                                       float left,
	                                       float bottom): Camera(pos, lookAt, glm::ortho(left, left, bottom, bottom)) {

	}
}
