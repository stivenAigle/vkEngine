if(UNIX)

	set(dependency glfw3 vulkan vkEngine utils Xcursor X11 Xxf86vm Xrandr pthread Xi dl Xi Xinerama)
	#SET(CMAKE_FIND_LIBRARY_SUFFIXES .so )
	find_path(VKENGINE_INCLUDE_DIR
			  NAMES glfw/glfw3.h vkEngine/

			  )

	foreach(depend ${dependency})

		find_library(${depend}_lib
					 NAMES ${depend})

		list(APPEND VKENGINE_LIBRARIES ${${depend}_lib})

	endforeach(depend)

endif(UNIX)

#set(VKENGINE_LIBRARIES ${VKENGINE_LIBRARY})
set(VKENGINE_INCLUDE_DIRS ${VKENGINE_INCLUDE_DIR})

