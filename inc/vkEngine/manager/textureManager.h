//
// Created by stiven on 17-10-20.
//

#ifndef GLENGINE_TEXTUREMANAGER_H
#define GLENGINE_TEXTUREMANAGER_H

#include <glish/Texture.hpp>
#include <vkEngine/vulkan/textureSampler.hpp>


#include "assetsList.h"
#include <vkEngine/vulkan/initVulkan.hpp>

namespace vkEngine {
    class TextureManager {

    private:
	    using Texture = vkEngine::TextureSampler;
        static TextureManager textureManager;
        AssetsList<Texture > textures;
        TextureManager();

    public:
        static int get(std::string && path);
        const Texture & get(int i)const;
        static void use(int i, int numberTexture);
	    static AssetsList<Texture> const & getAssetsList();
	    static Texture const & last();

	    friend void vkEngine::InitVulkan::terminate();
    };
}

#endif //GLENGINE_TEXTUREMANAGER_H
