//
// Created by stiven on 18-02-23.
//

#ifndef VKENGINE_ORTHOGRAPHICCAMERA_HPP
#define VKENGINE_ORTHOGRAPHICCAMERA_HPP

#include "camera.h"

namespace vkEngine {
	class OrthographicCamera : public Camera {


	public:
		OrthographicCamera(const glm::vec3 &pos,
		                   const glm::vec3 &lookAt,
		                   float left,
		                   float bottom);
	};
}


#endif //VKENGINE_ORTHOGRAPHICCAMERA_HPP
