//
// Created by stiven on 18-02-23.
//

#ifndef VKENGINE_PERSPECTIVECAMERA_HPP
#define VKENGINE_PERSPECTIVECAMERA_HPP

#include <vkEngine/display/camera.h>
#include <glm/glm.hpp>

namespace vkEngine {
	class PerspectiveCamera : public Camera {
		float aspect;
		float fov;
	public :
		PerspectiveCamera(const glm::vec3 &pos, const glm::vec3 &lookAt, float aspect);
	};
}


#endif //VKENGINE_PERSPECTIVECAMERA_HPP
