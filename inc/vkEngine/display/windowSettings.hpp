//
// Created by stiven on 28/12/17.
//

#ifndef VKENGINE_WINDOWSETTINGS_HPP
#define VKENGINE_WINDOWSETTINGS_HPP

#include <string>
namespace vkEngine {

    struct windowSettings{

        std::string title{"no title"};
        int width = 800;
        int height = 600;
        bool doubleBuffer = true;
        unsigned depthBits = 8;
        unsigned stencilBits = 1;
        unsigned majorVersion = 1;
        unsigned minorVersion = 0;
    };
}
#endif //GLENGINE_WINDOWSETTINGS_HPP
