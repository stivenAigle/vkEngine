//
// Created by stiven on 17-09-23.
//

#ifndef GLENGINE_PARSER_H
#define GLENGINE_PARSER_H

#include <string>
#include <vector>
#include <glm/glm.hpp>
#include <map>
#include <vkEngine/manager/materialManager.h>
#include <vkEngine/utils/utils.h>

namespace vkEngine {



    struct parsing_data {
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec3> normals;
        std::vector<glm::vec2> uvs;
        std::string pathTexture;
        std::map<std::string, material> materials;

        parsing_data() = default;


    };

    parsing_data const parseObj(std::string const &pathFile);
    void parseMtl(std::string const &path, mapMaterials &materials);
}
#endif //GLENGINE_PARSER_H
