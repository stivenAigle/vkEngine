//
// Created by darkblinx on 12/05/17.
//

#ifndef GLENGINE_VEC2_HPP
#define GLENGINE_VEC2_HPP

#include <type_traits>
#include <ostream>
#include <cmath>

namespace vkEngine{
    template<class T, class = std::enable_if_t<std::is_arithmetic<T>::value>>
    struct vec2 {
        T x = 0;
        T y = 0;

        /****************Constuctor and assignement**************************************/
        constexpr vec2() = default;
        constexpr vec2(T x):x(x), y(x){}
        constexpr vec2(T x, T y):x(x),y(y){}
        constexpr vec2(vec2 const &) = default;
        constexpr vec2(vec2 &&) = default;
        constexpr vec2 &operator=(vec2 const &) = default;
        constexpr vec2&operator=(vec2 &&) = default;
        /********************************************************************************/

        constexpr float getNorm(){
            return std::sqrt(x*x + y*y);
        }

        constexpr vec2 operator+=(vec2<T> a) {
            x += a.x;
            y += a.y;
            return *this;
        }
        constexpr vec2 operator-=(vec2<T> a) {
            x -= a.x;
            y -= a.y;
            return *this;
        }
        constexpr vec2 operator*=(T a) {
            x *= a;
            y *= a;
            return *this;
        }
        constexpr vec2 operator/=(T a) {
            x /= a;
            y /= a;
            return *this;
        }
    };

    template <class T, class U>
    constexpr auto operator+(vec2<T> a, vec2<U> b) {
        return vec2<decltype(a.x + b.x)>{a.x + b.x, a.y + b.y};
    }

    template <class T, class U>
    constexpr auto operator*(vec2<T> a, U b) {
        return vec2<decltype(a.x * b)>{a.x * b, a.y * b};
    }

    template <class T, class U>
    constexpr auto operator-(vec2<T> a, vec2<U> b) {
        return vec2<decltype(a.x - b.x)>{a.x - b.x, a.y - b.y};
    }

    template <class T, class U>
    constexpr auto operator/(vec2<T> a, U b) {
        return vec2<decltype(a.x / b)>{a.x / b, a.y / b};
    }

    template <class T>
    std::ostream & operator <<(std::ostream & stream, vec2<T> a){
        stream << "x: " << a.x << " y: " << a.y ;
        return stream;
    }
    using ivec2 = vec2<int>;
    using dvec2 = vec2<double>;
    using fvec2 = vec2<float>;
}

#endif //GLENGINE_VEC2_HPP
